Problem 1:

The trivial way to solve this problem is to just keep a 3D matrix and keep updating the blocks when updates come in. A slightly better method would be to keep a set of all cubes that have been updated with a non-zero value and then only iterate through those instead of the entire cube. Though, this being a hackerrank question, I found a lot of discussion about Fenwick or Binary Index Trees, which allows both operations (update as well as sum to be computed in O(log(n)) time for a one dimensional Fenwick tree and O(log(n)^3) for a 3 dimensional one. 

Depending on whether or not there are many more update operations relative to query or vice-versa; you could write this problem, with update being O(1) and query taking O(n^3) or vice versa depending on how you store the data. For example, if you want update to be O(1) you solve the problem the trivial way stated above. If you want update to take O(n^3) then you store the sum from (0,0,0) to (x,y,z) in each cube and then sum is O(1) because the sum is just cube[x2][y2][z2] - cube[x1][y1][z1].

The fenwick tree way seemed the most complex, and gave good runtime for both operations so I chose that approach.

The way a Fenwick tree makes each operation efficient is really interesting and probably necessary to understand to understand the code written.

In short, it takes advantage of the fact that using certain binary operations, you can create and traverse an implicit tree based on the indices of an array. It does this by storing recursively storing the sum of the value at the node and the sum of all nodes in its left subtree in each node. Then because of the binary property stated above, you can traverse the tree in log(n) time and compute the update as well as the sum operation in log(n) time (in the one dimensional case).

I probably did a bad job of explaining (and I think the explananation requires more than one paragraph, honestly), but here are the links I used to understand the problem.

http://stackoverflow.com/questions/11713562/3d-fenwick-tree#comment15539250_11713562

http://cs.stackexchange.com/questions/10538/bit-what-is-the-intuition-behind-a-binary-indexed-tree-and-how-was-it-thought-a

http://cs.stackexchange.com/questions/42811/what-are-the-main-ideas-used-in-a-fenwick-tree/42816#42816

http://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/

http://stackoverflow.com/questions/15395317/meaning-of-bitwise-and-of-a-positive-and-negative-number

How to run:
1. Clone git repo
2. Go to directory
3. Run npm start (you can get npm by running brew install node or going to https://nodejs.org/en/download/)
	a. to run unit tests, run npm test

How to test the cube summation:

Example based on Hackerrank test case:
curl -d '{"input":"2\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n2 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2"}' -H 'content-type:application/json' "http://localhost:3000/cubeSummation"

curl -d '{"input":"<your_own_string_here>"}' -H 'content-type:application/json' "http://localhost:3000/cubeSummation"

Unit tests are in: tests/cube_summation_test.js. Normally, I would use a test framework like Mocha, but for the problem size, basic asserts seemed sufficient.



Problem 2
The refqctored code is in php/problem_2.php
1. The hardcoded error numbers and status ids are error prone and not descriptive. The error handling flow makes the code harder to read. There is an extra call to the database before making the push notification object. There are comments everywhere that might confuse the next person who touches the code. I removed them because if you need to see what used to exist in a file you should just look at the git history.
2. I added constants with descriptive variable names so that the code reads easier and gives more context as to the desired effects. I do all error handling at the beginning so it's very easy to see what error cases we're looking for and makes the flow of the rest of the code much easier to read. I took out the extra call to the database by reassigning servicio on line 35. I took out the unnecessary commented out code.



Problem 3
1. Every module or class should have responsibility over a single part of the functionality provided by the software and that responsibility should be fully taken care of by that module/class. 

I want to learn how software can have an impact in the developing world. I think it has more opportunity there than in the developed nations because in developed nations everything must be solved by software since labor costs are so high, whereas in developing nations companies can leverage both software as well as cheap labor to deliver services in a faster, higher quality, cheaper way than possible in developed nations. This seems like a huge and exciting opportunity.

2. Good code is something that does not reinvent the wheel. Good code uses pre-existing libraries, SDKs, etc. Good code is easily extendable, easily understood, and tested thoroughly. Good code is abstracted at the appropriate levels. Good code is also complex when it has to be, especially if it is a huge win for the user. 