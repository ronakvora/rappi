const NO_ERROR = '0';
const DRIVER_UNAVAILABLE_ERROR = '1';
const INVALID_STATUS_ID_ERROR = '2';
const UNABLE_TO_FIND_SERVICE_ERROR = '3';

const DRIVER_STATUS_AVAILABLE = '1';
const DRIVER_STATUS_BUSY = '2';

const UNAVAILABLE = '0';

const INVALID_STATUS_ID = '6';

const USER_TYPE_IOS = '1';

public function post_confirm() {
	$id = Input::get('service_id');
	$servicio = Service::find($id);
	if ($servicio == NULL) {
	  return Response::json(array('error' => UNABLE_TO_FIND_SERVICE));
	}
	if ($servicio->status_id == INVALID_STATUS_ID) {
    return Response::json(array('error' => INVALID_STATUS_ID_ERROR));
	}
	if ($servicio->driver_id != NULL || $servicio->status_id != DRIVER_STATUS_AVAILABLE) {
		return Response::json(array('error' => DRIVER_UNAVAILABLE_ERROR));
	}
	$servicio = Service::update($id, array(
	            'driver_id' => Input::get('driver_id'),
	            'status_id' => DRIVER_STATUS_BUSY
	));
	Driver::update(Input::get('driver_id'), array(
	    "available" => UNAVAILABLE
	));
	$driverTmp = Driver::find(Input::get('driver_id'));
	$servicio = Service::update($id, array(
	    'car_id' => $driverTmp->car_id
	));
	// Notificar a usuario!
	$pushMessage = 'Tu servicio ha sido confirmado!';
	$push = Push::make();
	if (servicio->user->uuid == '') {
	  return Response::json(array('error' => NO_ERROR));
	}
	if (servicio->user->type == USER_TYPE_IOS) {
	  $result = $push->ios($servicio->user->uuid; $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
	} else {
	  $result = $push->ios($servicio->user->uuid; $pushMessage, 1, 'default', 'Open', array('serviceId' => $servicio->id));
	}
	return Response::json(array('error' => NO_ERROR));
}