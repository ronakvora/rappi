// {} shift alt l for pipe 
var init_3D_array = function(array, N) {
	for(var i = 0; i <= N; i += 1) {
		array[i] = [];
    for(var j = 0; j <= N; j += 1) {
    	array[i][j] = [];
      for(var k = 0; k <= N; k += 1) {
      	array[i][j][k] = 0;
      }
    }
  }
}
var update = function(fenwick_cube, x, y, z, diff, N) {
	for (var i = x; i <= N; i += i & (-i)) {
		for (var j = y; j <= N; j += j & (-j)) {
			for (var k = z; k <= N; k += k & (-k)) {
				fenwick_cube[i][j][k] = fenwick_cube[i][j][k] + diff;
			}		
		}
	}
} 

var sum_up_to_point = function(fenwick_cube, x, y, z) {
  var sum = 0;
  for(var i = x; i > 0; i -= i & (-i)){
    for(var j = y; j > 0; j -= j & (-j)){
      for(var k = z; k > 0; k -= k & (-k)){
        sum += fenwick_cube[i][j][k];	
      }
    }
  }
  return sum;
}

var process_cube_summation = function(input) {
	sum_string = "";
	if (input === null || input === "") {
		// respond with error messqge
		return "invalid input string";
	}
	var input_lines = input.split('\n');
	var T_string = input_lines[0];
	var T = parseInt(T_string);
	if (T < 1 || T > 50) {
		return "T out of bounds. Needs to be between 1 and 50 inclusive; but is: " + T;
	}
	var line_num = 1
	while (T > 0) {
		var N_M_string = input_lines[line_num];
		line_num = line_num + 1;
		var N_M_array = N_M_string.split(' ');
		var N_string = N_M_array[0];
		var M_string = N_M_array[1];
		var N = parseInt(N_string);
		var M = parseInt(M_string);
		if (N < 1 || N > 100) {
		  return "N must be between 1 and 100 inclusive but is: " + N;
	  }
	  if (M < 1 || M > 1000) {
		  return "M must be between 1 and 1000 inclusive but is: " + M;
	  }
		// fenwick cube is used to store sums efficiently 
		var fenwick_cube = [];
		// cache for current value of each cube so that you do not need to recompute sum
		// of only this block when executing update
		var normal_cube = [];
		init_3D_array(fenwick_cube, N);
		init_3D_array(normal_cube, N);
		while (M > 0) {
			var operation_string = input_lines[line_num];
			line_num = line_num + 1;
			var operation_array = operation_string.split(' ');
			var operation = operation_array[0]
			if (operation === "UPDATE") {
				var x = parseInt(operation_array[1]);
				var y = parseInt(operation_array[2]);
				var z = parseInt(operation_array[3]);
				var W = parseInt(operation_array[4]);
				if (x < 1 || x > N) {
		  		return "Error in update string. x must be between 1 and N. x is " + x + ", N is " + N;
	  		}
	  		if (y < 1 || y > N) {
		  		return "Error in update string. y must be between 1 and N. y is " + y + ", N is " + N;
	  		}
	  		if (z < 1 || z > N) {
		  		return "Error in update string. z must be between 1 and N. z is " + z + ", N is " + N;
	  		}
	  		if (W < -1000000000 || W > 1000000000) {
		  		return "Error in update string. W must be between -1000000000 and 1000000000 inclusive";
	  		}
				var diff = W - normal_cube[x][y][z];
				update(fenwick_cube, x, y, z, diff, N);
				normal_cube[x][y][z] = W;
			} else {
				var x1 = parseInt(operation_array[1]);
				var y1 = parseInt(operation_array[2]);
				var z1 = parseInt(operation_array[3]);
				var x2 = parseInt(operation_array[4]);
				var y2 = parseInt(operation_array[5]);
				var z2 = parseInt(operation_array[6]);
				if (x1 < 1 || x1 > N) {
		  		return "Error in update string. x1 must be between 1 and N. x1 is " + x1 + ", N is " + N;
	  		}
	  		if (y1 < 1 || y1 > N) {
		  		return "Error in update string. y1 must be between 1 and N. y1 is " + y1 + ", N is " + N;
	  		}
	  		if (z1 < 1 || z1 > N) {
		  		return "Error in update string. z1 must be between 1 and N. z1 is " + z1 + ", N is " + N;
	  		}
	  		if (x2 < 1 || x2 > N) {
		  		return "Error in update string. x2 must be between 1 and N. x2 is " + x2 + ", N is " + N;
	  		}
	  		if (y2 < 1 || y2 > N) {
		  		return "Error in update string. y2 must be between 1 and N. y2 is " + y2 + ", N is " + N;
	  		}
	  		if (z2 < 1 || z2 > N) {
		  		return "Error in update string. z2 must be between 1 and N. z2 is " + z2 + ", N is " + N;
	  		}
				// cube arithmetic to dedupe and add up correct precomputed sums
				var sum_x2y2z2 = sum_up_to_point(fenwick_cube, x2, y2, z2) - sum_up_to_point(fenwick_cube, x1-1, y2, z2) - sum_up_to_point(fenwick_cube, x2, y1-1, z2) + sum_up_to_point(fenwick_cube, x1-1, y1-1, z2);
        		var sum_x1y1z1 = sum_up_to_point(fenwick_cube, x2, y2, z1-1) - sum_up_to_point(fenwick_cube, x1-1, y2, z1-1) - sum_up_to_point(fenwick_cube, x2, y1-1, z1-1)  + sum_up_to_point(fenwick_cube, x1-1, y1-1, z1-1);
				var sum = sum_x2y2z2 - sum_x1y1z1;
				if (sum_string.length > 0) {
					sum_string += "\n";
				}
				sum_string += sum;
			}
			M = M - 1;
		}
		T = T - 1;  
	}
	return sum_string;
}
exports.process_cube_summation = process_cube_summation;

var express = require('express')
var bodyParser = require('body-parser')
var app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
})); 

app.post('/cubeSummation', function (req, res) {
	res.send(process_cube_summation(req.body.input));
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
