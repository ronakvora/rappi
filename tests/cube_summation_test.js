var process_cube_summation = require('../cube_summation').process_cube_summation

var assert = require('assert')

// test hackerrank
var hackerrank_test_actual = process_cube_summation("2\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\n2 4\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2");
var hackerrank_test_expected = "4\n4\n27\n0\n1";

// test boundary conditions
var boundary_test_T_min_actual = process_cube_summation("0\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_T_min_expected = "T out of bounds. Needs to be between 1 and 50 inclusive; but is: 0"
assert.equal(boundary_test_T_min_actual, boundary_test_T_min_expected, "T min boundary condition check failed");

var boundary_test_T_max_actual = process_cube_summation("51\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_T_max_expected = "T out of bounds. Needs to be between 1 and 50 inclusive; but is: 51"
assert.equal(boundary_test_T_max_actual, boundary_test_T_max_expected, "T max boundary condition check failed");

var boundary_test_N_min_actual = process_cube_summation("1\n0 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_N_min_expected = "N must be between 1 and 100 inclusive but is: 0";
assert.equal(boundary_test_N_min_actual, boundary_test_N_min_expected, "N min boundary condition check failed")

var boundary_test_N_max_actual = process_cube_summation("1\n101 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_N_max_expected = "N must be between 1 and 100 inclusive but is: 101";
assert.equal(boundary_test_N_max_actual, boundary_test_N_max_expected, "N max boundary condition check failed")

var boundary_test_M_min_actual = process_cube_summation("1\n4 0\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_M_min_expected = "M must be between 1 and 1000 inclusive but is: 0"
assert.equal(boundary_test_M_min_actual, boundary_test_M_min_expected, "M min boundary condition check failed")

var boundary_test_M_max_actual = process_cube_summation("1\n4 1001\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_M_max_expected = "M must be between 1 and 1000 inclusive but is: 1001"
assert.equal(boundary_test_M_max_actual, boundary_test_M_max_expected, "M max boundary condition check failed")

var boundary_test_x_min_actual = process_cube_summation("1\n4 5\nUPDATE 0 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_x_min_expected = "Error in update string. x must be between 1 and N. x is 0, N is 4"
assert.equal(boundary_test_x_min_actual, boundary_test_x_min_expected, "x min boundary condition check failed")

var boundary_test_x_max_actual = process_cube_summation("1\n4 5\nUPDATE 6 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_x_max_expected = "Error in update string. x must be between 1 and N. x is 6, N is 4"
assert.equal(boundary_test_x_max_actual, boundary_test_x_max_expected, "x max boundary condition check failed")

var boundary_test_y_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 0 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_y_min_expected = "Error in update string. y must be between 1 and N. y is 0, N is 4"
assert.equal(boundary_test_y_min_actual, boundary_test_y_min_expected, "y min boundary condition check failed")

var boundary_test_y_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 6 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_y_max_expected = "Error in update string. y must be between 1 and N. y is 6, N is 4"
assert.equal(boundary_test_y_max_actual, boundary_test_y_max_expected, "y max boundary condition check failed")

var boundary_test_z_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 0 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_z_min_expected = "Error in update string. z must be between 1 and N. z is 0, N is 4"
assert.equal(boundary_test_z_min_actual, boundary_test_z_min_expected, "z min boundary condition check failed")

var boundary_test_z_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 6 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_z_max_expected = "Error in update string. z must be between 1 and N. z is 6, N is 4"
assert.equal(boundary_test_z_max_actual, boundary_test_z_max_expected, "z max boundary condition check failed")

var boundary_test_W_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 -1000000001\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_W_min_expected = "Error in update string. W must be between -1000000000 and 1000000000 inclusive"
assert.equal(boundary_test_W_min_actual, boundary_test_W_min_expected, "W boundary condition check failed")

var boundary_test_W_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 1000000001\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_W_max_expected = "Error in update string. W must be between -1000000000 and 1000000000 inclusive"
assert.equal(boundary_test_W_max_actual, boundary_test_W_max_expected, "W boundary condition check failed")

var boundary_test_x1_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 0 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_x1_min_expected = "Error in update string. x1 must be between 1 and N. x1 is 0, N is 4"
assert.equal(boundary_test_x1_min_actual, boundary_test_x1_min_expected, "x1 min boundary condition check failed")

var boundary_test_x1_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 6 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_x1_max_expected = "Error in update string. x1 must be between 1 and N. x1 is 6, N is 4"
assert.equal(boundary_test_x1_max_actual, boundary_test_x1_max_expected, "x1 max boundary condition check failed")

var boundary_test_y1_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 0 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_y1_min_expected = "Error in update string. y1 must be between 1 and N. y1 is 0, N is 4"
assert.equal(boundary_test_y1_min_actual, boundary_test_y1_min_expected, "y1 min boundary condition check failed")

var boundary_test_y1_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 6 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_y1_max_expected = "Error in update string. y1 must be between 1 and N. y1 is 6, N is 4"
assert.equal(boundary_test_y1_max_actual, boundary_test_y1_max_expected, "y1 max boundary condition check failed")

var boundary_test_z1_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 0 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_z1_min_expected = "Error in update string. z1 must be between 1 and N. z1 is 0, N is 4"
assert.equal(boundary_test_z1_min_actual, boundary_test_z1_min_expected, "z1 min boundary condition check failed")

var boundary_test_z1_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 6 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_z1_max_expected = "Error in update string. z1 must be between 1 and N. z1 is 6, N is 4"
assert.equal(boundary_test_z1_max_actual, boundary_test_z1_max_expected, "z1 max boundary condition check failed")

var boundary_test_x2_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 0 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_x2_min_expected = "Error in update string. x2 must be between 1 and N. x2 is 0, N is 4"
assert.equal(boundary_test_x2_min_actual, boundary_test_x2_min_expected, "x2 min boundary condition check failed")

var boundary_test_x2_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 6 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_x2_max_expected = "Error in update string. x2 must be between 1 and N. x2 is 6, N is 4"
assert.equal(boundary_test_x2_max_actual, boundary_test_x2_max_expected, "x2 max boundary condition check failed")

var boundary_test_y2_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 0 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_y2_min_expected = "Error in update string. y2 must be between 1 and N. y2 is 0, N is 4"
assert.equal(boundary_test_y2_min_actual, boundary_test_y2_min_expected, "y2 min boundary condition check failed")

var boundary_test_y2_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 6 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_y2_max_expected = "Error in update string. y2 must be between 1 and N. y2 is 6, N is 4"
assert.equal(boundary_test_y2_max_actual, boundary_test_y2_max_expected, "y2 max boundary condition check failed")

var boundary_test_z2_min_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 0\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_z2_min_expected = "Error in update string. z2 must be between 1 and N. z2 is 0, N is 4"
assert.equal(boundary_test_z2_min_actual, boundary_test_z2_min_expected, "z2 min boundary condition check failed")

var boundary_test_z2_max_actual = process_cube_summation("1\n4 5\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 6\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3");
var boundary_test_z2_max_expected = "Error in update string. z2 must be between 1 and N. z2 is 6, N is 4"
assert.equal(boundary_test_z2_max_actual, boundary_test_z2_max_expected, "z2 max boundary condition check failed")

// test complicated
var complex_test_actual = process_cube_summation("1\n4 9\nUPDATE 2 2 2 4\nQUERY 1 1 1 3 3 3\nUPDATE 1 1 1 23\nQUERY 2 2 2 4 4 4\nQUERY 1 1 1 3 3 3\nUPDATE 2 2 2 1\nQUERY 1 1 1 1 1 1\nQUERY 1 1 1 2 2 2\nQUERY 2 2 2 2 2 2");
var complex_test_expected = "4\n4\n27\n23\n24\n1"
assert.equal(complex_test_actual, complex_test_expected, "complex cube test failed")

console.log("All tests passed!");
